# discountbot

Find users needing a discount and send it through email.

### Quick Install

Install discountbot from PyPI

        # pip3 install discountbot

Install discountbot from sources

        # tar zxvf discountbot-0.1.tar.gz
        # cd discountbot
        # python3 setup.py install
        # # or
        # python3 setup.py install --install-scripts=/usr/bin

### Configure discountbot

Create or modify discountbot.ini file in order to configure discountbot:

        [discount]
        code=MYBIGCOMPANY-WELCOME
        duration=2
        
        [database]
        db_name=customers
        db_user=customers_usr
        db_pass=V3rYS3cR3tP4sSw0rd,
        sql_get_users=select custome.email from customerinfos where customerinfos.id not in (select customer_id from payments)
        sql_create_discount=insert into discounts (code,percentage,start,end,status,created) values ('{code}',20.00,'{start}','{end}',1,'{created}')
        
        [email]
        email_subject=Your Incredible Discount!
        email_from=sales@mybigcompany.com
        
For readability some parameters are not in the file, so their defaut values are used. [Read the configure section](https://discountbot.readthedocs.org/en/latest/configure.html) of the official documentation for the default value of the different parameters.

### Use discountbot

Launch discountbot

        $ discountbot -c /path/to/discountbot.ini

### Authors

Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+. See the LICENSE file for the complete text of the license.
