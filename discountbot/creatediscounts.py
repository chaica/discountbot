# Copyright © 2017 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Create the discounts'''

# standard libraires imports
from datetime import date, datetime, timedelta
import logging

# 3rd parties imports
from sqlalchemy import create_engine
from sqlalchemy import text

# app libraries imports
from discountbot.storesqldiscountids import StoreSQLDiscountIds

def create_discounts(confs, emails):
    '''Create discounts'''
    sqltext = '{dbconnector}://{dbuser}:{dbpass}@{dbhost}/{dbname}'
    request = sqltext.format(dbconnector=confs['database']['connector'],
                                dbuser=confs['database']['user'],
                                dbpass=confs['database']['pass'],
                                dbhost=confs['database']['host'],
                                dbname=confs['database']['name'])
    engine = create_engine(request)
    sql = text(confs['database']['sql_create_discount'])
    emails = emails
    confs = confs
    sendingto = []
    sqlitestore = StoreSQLDiscountIds(confs['sqlite'])
    for i, email in enumerate(emails):
        code = '{code}-{date}{indice}'.format(code=confs['discount']['code'], date=datetime.now().strftime('%Y%m%d%H%S'), indice=i)
        logging.debug('Discount code: {code}'.format(code=code))
        start=date.today().strftime('%Y-%m-%d')
        logging.debug('Discount startdate: {startdate}'.format(startdate=start))
        end=(date.today() + timedelta(days=confs['discount']['duration'])).strftime('%Y-%m-%d')
        logging.debug('Discount enddate: {enddate}'.format(enddate=end))
        created=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        logging.debug('Discount created datetime: {created}'.format(created=created))
        sqlrequest = confs['database']['sql_create_discount'].format(code=code, start=start, end=end, created=created)
        sqlresult = engine.execute(sqlrequest)
        lastinsertedid = engine.execute('select LAST_INSERT_ID()').first()[0]
        sqlitestore.store_sql_discount_id(email, lastinsertedid)
        sendingto.append({'email': email, 'code': code, 'sqlcodeid': lastinsertedid})
    return sendingto
