# -*- coding: utf-8 -*-
# Copyright © 2018 Carl Chenet <chaica@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Check placeholders in the SQL request to create the discount
'''check placeholders in the SQL request to create the discount'''

# standard library imports
import logging
from re import findall
import sys

def check_placeholders(sql):
    '''check the plaholders'''
    userplaceholders = [] 
    supportedplaceholders = ('code', 'id', 'start', 'end', 'created', 'discountid', 'duration')

    logging.debug('sql request: {sql}'.format(sql=sql))
    for word in findall('{[^{}]*}', sql):
        placeholder = word.strip('{}')
        userplaceholders.append(placeholder)
    logging.debug('Found user-defined placeholders: {userplaceholders}'.format(userplaceholders=userplaceholders))
    for userplaceholder in userplaceholders:
        if userplaceholder not in supportedplaceholders:
            sys.exit('The placeholder {userplaceholder} is not supported. The available ones are: {supportedplaceholders}'.format(userplaceholder=userplaceholder, supportedplaceholders=supportedplaceholders))
