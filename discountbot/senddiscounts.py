# -*- coding: utf-8 -*-
# Copyright © 2017 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# SendDiscounts class
'''SendDiscounts class'''

import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_discounts(confs, contacts):
    with open(confs['email']['template_path']) as tpl:
        templatecontent = tpl.read()
        for contact in contacts:
                html = templatecontent
                html = html.replace('{code}',contact['code'])
                html = html.replace('{duration}', str(confs['discount']['duration']))
                try:
                    msg = MIMEMultipart('alternative')
                    html = MIMEText(html, 'html')
                    msg.attach(html)
                    msg['Subject'] = confs['email']['subject']
                    msg['From'] = confs['email']['from']
                    msg['To'] = contact['email']
                    if 'hidden_copy_to' in confs['email']:
                        msg['Bcc'] = confs['email']['hidden_copy_to']
                    if 'port' in confs['email']:
                        s = smtplib.SMTP(confs['email']['host'], port=confs['email']['port'])
                    else:
                        s = smtplib.SMTP(confs['email']['host'])
                    if 'user' in confs['email'] and 'pass' in confs['email']:
                        s.login(confs['email']['user'], confs['email']['pass'])
                    logging.debug('Sending message from {msgfrom} to {msgto} with subject {msgsubject}'.format(msgfrom=msg['From'], msgto=msg['To'], msgsubject=msg['Subject']))
                    s.send_message(msg)
                    s.quit()
                except smtplib.SMTPException as err:
                    logging.error('smtplib error: {}'.format(err))
