# -*- coding: utf-8 -*-
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file for the Redis server
'''Get values of the configuration file for the Redis server'''

# standard library imports
import sys

#app libraries imports
from discountbot.checkplaceholders import check_placeholders

def parse_database(config):
   # database section
    section = 'database'
    sqlcreatediscount = 'sql_create_discount'
    sqlremovediscount = 'sql_remove_discount'
    if config.has_section(section):
        databaseconf = {}
        databaseconf['connector'] = 'mysql+mysqlconnector'
        databaseconf['host'] = 'localhost'
        databaseconf['port'] = 3306
        databaseconf['request'] = 'update {table} set {sent_column} = {sent_value}, {retweets_column} = {retweets_value}, {likes_column} = {likes_value} where id = {dbid}'
        possibleoptions = ['connector', 'host', 'name', 'user', 'pass', 'sql_get_users', sqlcreatediscount, sqlremovediscount]
        for cfgkey in possibleoptions:
            if config.has_option(section, cfgkey):
                databaseconf[cfgkey] = config.get(section, cfgkey)
                if cfgkey == sqlcreatediscount or cfgkey == sqlremovediscount:
                    check_placeholders(config.get(section, cfgkey))
    return databaseconf
