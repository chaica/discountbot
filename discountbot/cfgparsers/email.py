# -*- coding: utf-8 -*-
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file to send the email
'''Get values of the configuration file to send the email'''

def parse_email(config):
   # email section
    section = 'email'
    if config.has_section(section):
        emailconf = {}
        if config.has_section(section):
            cfgkey = 'template_path'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            else:
                emailconf[cfgkey] = '/usr/share/discountbot/discountbot.template.txt'
            cfgkey = 'host'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            else:
                emailconf[cfgkey] = 'localhost'
            cfgkey = 'port'
            if config.has_option(section, cfgkey):
                try:
                    portcfg = config.get(section, cfgkey)
                    emailconf[cfgkey] = int(portcfg)
                except ValueError as _:
                    emailconf[cfgkey] = 25
            cfgkey = 'from'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            else:
                emailconf[cfgkey] = 'localhost'
            cfgkey = 'subject'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            else:
                emailconf[cfgkey] = 'Your Discount!'
            cfgkey = 'user'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            cfgkey = 'pass'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
            cfgkey = 'hidden_copy_to'
            if config.has_option(section, cfgkey):
                emailconf[cfgkey] = config.get(section, cfgkey)
    return emailconf
