# Copyright © 2017-2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Main class of DiscountBot'''

# standard libraires imports
import logging
import sys

# app libraries imports
from discountbot.parsecli import parse_cli
from discountbot.parsecfg import parse_cfg
from discountbot.checkalreadysent import check_already_sent
from discountbot.creatediscounts import create_discounts
from discountbot.extractemails import extract_emails
from discountbot.removediscounts import remove_discounts
from discountbot.senddiscounts import send_discounts

class Main:
    '''Main class of DiscountBot'''

    def __init__(self):
        self.main()

    def main(self):
        '''The main function'''
        cliopts = parse_cli()
        logging.basicConfig(level=cliopts.log_level.upper(), format='%(message)s')
        # iterating over the different configuration files
        cfg = parse_cfg(cliopts.config)
        print(cfg['database'])
        emails = extract_emails(cfg['database'])
        if emails:
            logging.debug('Customers to send discounts are in the database: {emails}'.format(emails=emails))
            # check if we notified the users already
            emails = check_already_sent(cfg['sqlite'], emails)
            if not emails:
                logging.debug('No new customer to send discount to. Leaving.')
            else:
                logging.debug('Will send discounts to : {emails}'.format(emails=emails))
            # create discounts
            if not cliopts.populate and emails:
                contacts = create_discounts(cfg, emails)
                if contacts:
                    send_discounts(cfg, contacts)            
        logging.debug('Will remove old discounts')
        if not cliopts.dryrun and 'sql_remove_discount' in cfg['database']:
            remove_discounts(cfg)    
        sys.exit(0)
