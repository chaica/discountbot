# Copyright © 2017 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Store Discount ids into the sqlite database'''

# 3rd parties imports
from sqlalchemy import create_engine
from sqlalchemy import text

class StoreSQLDiscountIds:
    def __init__(self, sqliteconf):
        '''Store SQL discount id'''
        sqlstring = 'sqlite:///{sqlitepath}'.format(sqlitepath=sqliteconf['sqlite_path'])
        self.engine = create_engine(sqlstring)

    def store_sql_discount_id(self, email, discountid):
        '''store a discount id'''
        sqlrequest = 'update wasemailedalready set discountid = {discountid} where email = "{email}"'.format(discountid=discountid, email=email)
        sqlresult = self.engine.execute(sqlrequest)
