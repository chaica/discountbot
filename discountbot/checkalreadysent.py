# Copyright © 2017 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''CheckSentAlready class'''

# standard libraires imports
import logging
import os
import sys

# 3rd parties libraries imports
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# app libraries imports
from discountbot.wasemailedalready import WasEmailedAlready

def check_already_sent(sqliteconf, emails):
        engine = create_engine('sqlite:///{sqlitepath}'.format(sqlitepath=sqliteconf['sqlite_path']))
        tmpsession = sessionmaker(bind=engine)
        session = tmpsession()
        session = session
        WasEmailedAlready.metadata.create_all(engine)
        sendingto = []
        sqliteobj = session.query(WasEmailedAlready.email)
        alreadycontactedemails = [i[0] for i in sqliteobj if len(i) > 0]
        for email in emails:
            if email not in alreadycontactedemails:
                sendingto.append(email)
                newemail = WasEmailedAlready(email=email)
                session.add(newemail)
        session.commit()
        return sendingto
