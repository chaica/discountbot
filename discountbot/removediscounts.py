# Copyright © 2017-2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Removed old discounts'''

# standard libraires imports
import logging

# 3rd parties imports
from sqlalchemy import create_engine
from sqlalchemy import text

def remove_discounts(confs):
    '''Create discounts'''
    sqlconnectorstring = '{dbconnector}://{dbuser}:{dbpass}@{dbhost}/{dbname}'
    sqlrequest = sqlconnectorstring.format(dbconnector=confs['database']['connector'],
                                dbuser=confs['database']['user'],
                                dbpass=confs['database']['pass'],
                                dbhost=confs['database']['host'],
                                dbname=confs['database']['name'])
    sqlengine = create_engine(sqlrequest)
    #sqlengine.execute(sql)
    sqliteconnector = 'sqlite:///{sqlitepath}'.format(sqlitepath=confs['sqlite']['sqlite_path'])
    sqliteengine = create_engine(sqliteconnector)
    sqliterequest = text('select discountid from wasemailedalready')
    sqliteresult = sqliteengine.execute(sqliterequest)
    for sqliterow in sqliteresult:
        if sqliterow and len(sqliterow) == 1:
            discountid = sqliterow[0]
            sqlrequest = text(confs['database']['sql_remove_discount'].format(duration=confs['discount']['duration'], discountid=discountid))
            sqlengine.execute(sqlrequest)
