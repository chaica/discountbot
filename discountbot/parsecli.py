# -*- coding: utf-8 -*-
# Copyright © 2017-2018 Carl Chenet <chaica@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# CLI parsing
'''CLI parsing'''

# standard library imports
from argparse import ArgumentParser

__version__ = '0.2'

def parse_cli():
    '''Parse the CLI'''
    epilog = 'For more information: https://discountbot.readthedocs.org'
    description = 'Offer a discount after some days' 
    parser = ArgumentParser(prog='discountbot',
                            description=description,
                            epilog=epilog)
    parser.add_argument('--version', action='version', version=__version__)
    parser.add_argument('-c', '--config',
                        default='/etc/discountbot/discountbot.ini',
                        help='Location of config file (default: %(default)s)',
                        metavar='FILE')
    parser.add_argument('-n', '--dry-run', dest='dryrun',
                        action='store_true', default=False,
                        help='Do not actually perform operations, just simulate')
    parser.add_argument('-v', '--verbose', '--info', dest='log_level',
                        action='store_const', const='info', default='warning',
                        help='enable informative (verbose) output, work on log level INFO')
    parser.add_argument('-d', '--debug', dest='log_level',
                        action='store_const', const='debug', default='warning',
                        help='enable debug output, work on log level DEBUG')
    parser.add_argument('-p', '--populate', action='store_true', default=False,
                        dest='populate',
                        help='retrieve and store entries in sqlite according to the sql_get_users request. Do nothing more.')
    opts = parser.parse_args()
    return opts
