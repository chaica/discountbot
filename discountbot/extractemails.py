# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Extract the users'''

# 3rd parties imports
from sqlalchemy import create_engine
from sqlalchemy import text

def extract_emails(dbconf):
    '''Extract the emails'''
    request = '{dbconnector}://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(dbconnector=dbconf['connector'],
                                                                            dbuser=dbconf['user'],
                                                                            dbpass=dbconf['pass'],
                                                                            dbhost=dbconf['host'],
                                                                            dbname=dbconf['name'])
    engine = create_engine(request)
    sql = text(dbconf['sql_get_users'])
    emails = []
    sqlresult = engine.execute(sql)
    for line in sqlresult:
        emails.append(line[0])
    return emails
