'''WasEmailedAlready mapping for SQLAlchemy'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean

MYBASE = declarative_base()

class WasEmailedAlready(MYBASE):
    '''WasEmailedAlready mapping for SQLAlchemy'''
    __tablename__ = 'wasemailedalready'
    email = Column(String(254), primary_key=True)
    discountid = Column(Integer())
