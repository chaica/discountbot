# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Setup for DiscountBot
'''Setup for DiscountBot'''

from setuptools import setup, find_packages

CLASSIFIERS = [
    'Intended Audience :: End Users/Desktop',
    'Environment :: Console',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6'
]


setup(
    name='discountbot',
    version='0.2',
    license='GNU GPL v3',
    description='Find customers needing a discount and send it through email',
    long_description='Find customers needing a discount and send it through email',
    author = 'Carl Chenet',
    author_email = 'chaica@ohmytux.com',
    url = 'https://gitlab.com/chaica/discountbot',
    classifiers=CLASSIFIERS,
    download_url='https://gitlab.com/chaica/discountbot',
    packages=find_packages(),
    scripts=['scripts/discountbot'],
    install_requires=['sqlalchemy'],
    extras_require={
        'mysql':  ['mysql_connector'],
        'postgresql':  ['psycopg2'],
    }
)
