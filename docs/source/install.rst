How to install discountbot
==========================

From PyPI
^^^^^^^^^
You need to define what the SQL database backend you will use, then use the following command:

* for MySQL
    $ pip3 install discountbot[mysql]

* for PostgreSQL
    $ pip3 install discountbot[postgresql]

From sources
^^^^^^^^^^^^
* You need at least Python 3.4.
* Untar the tarball and go to the source directory with the following commands::

    $ tar zxvf discountbot-0.1.tar.gz
    $ cd discountbot

* Next, to install discountbot on your computer, type the following command with the root user::

    $ python3 setup.py install
    $ # or
    $ python3 setup.py install --install-scripts=/usr/bin

