Documentation for the discountbot project
=========================================

Discountbot automatically finds who need a discount, create the discounts and send emails with the discount code in it. Discountbot needs a SQL request to get the emails, a SQL request to create the discounts and an email template to send the emails.

You'll find below anything you need to install, configure or run discountbot.

Guide
=====

.. toctree::
   :maxdepth: 2

   install
   configure
   use
   license
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

