Initial launch of discountbot
=============================
After you defined the configuration of discountbot, while launching discountbot for the first time, you could be interested in NOT sending discounts to all of the identified customers, because maybe you already sent them discounts manually. For that, just use the -p or --populate option::

    $ discountbot -p

While launching discountbot this way, the configuration file should be located at /etc/discountbot/discountbot.ini

Change the default location of the discountbot configuration
============================================================
Or you can change the default location of the configuration file (/etc/discountbot/discountbot.ini) with the -c option::

    $ discountbot -c /opt/discountbot/discountbot.ini

Dry run mode
============
The dry run mode allows for simulating the creations of discounts and sending them to the users. It does not perform any definitive actions, like storing the users or sending them real emails::

    $ discount --dry-run

Debug mode
==========
Using the -d or --debug option, you activate the debug mode, increasing verbosity about what's going on::

    $ discountbot -d
